const express = require('express');
const router = express.Router();

const Registro = require('../models/registro');

router.get('/', async (req, res) => {
    const registros = await Registro.find();
    res.render('index', {
        registros
    });
});

router.post('/add', async (req, res, next) => {
    const registro = new Registro(req.body);
    await registro.save();
    res.redirect('/');
});

router.get('/edit/:id', async (req, res, next) => {
    const { id } = req.params;
    const registro = await Registro.findById(req.params.id);
    res.render('edit', { 
        registro 
    });
});

router.post('/edit/:id', async (req, res, next) => {
    const { id } = req.params;
    await Registro.update({_id: id}, req.body);
    res.redirect('/');
  });

router.get('/delete/:id', async (req, res, next) => {
    const { id } = req.params;
    await Registro.remove({_id: id});
    res.redirect('/');
});


module.exports = router;