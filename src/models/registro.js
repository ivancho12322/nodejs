const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RegistroSchema = Schema({
    nombre : String,
    apellido : String,
    pago : Number
  });

  module.exports = mongoose.model('registros', RegistroSchema);